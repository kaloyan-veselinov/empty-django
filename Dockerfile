FROM python:3.7-slim-buster

# Requirements are installed here to ensure they will be cached.
COPY requirements.txt /requirements/
RUN pip install --no-cache-dir -r /requirements/requirements.txt

COPY . /app

ENV STATIC_ROOT /var/www/app/static/
ENV MEDIA_ROOT /var/www/app/media/
ENV SQLITE3_PATH /var/www/app/db.sqlite3

WORKDIR /app

EXPOSE "8000"

ENTRYPOINT ["python", "manage.py"]

CMD ["runserver", "0.0.0.0:8000"]